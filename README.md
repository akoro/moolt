# moolt

> A 2HP buffered Eurorack multiple that duplicates 2 audio or control voltages 3 times each or 1 signal 6 times.

## status

The schematics, PCB and panel are mostly done. I suspect the drill holes need alignment, but then the first batch of PCBs and panel will be ordered from JLCPCB. I'll do the first round of extensive testing, make the necessary changes and either do another small order for testing or a full production run.